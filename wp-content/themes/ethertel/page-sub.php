<?php
/**
 * Template Name: 2 Column
 */

get_header(); ?>

<div class="container2">
	<div id="left">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>
		<?php edit_post_link(); ?>
	<?php endwhile; // end of the loop. ?>
    </div>

<div id="right">
    <div id="tslider" style="margin-top:70px;">
        <ul class="tst">
            <?php query_posts( array( 'cat'=>'2', 'showposts' => 5 ) );
                if ( have_posts() ) : while ( have_posts() ) : the_post();
            ?>
            <li><?php the_content(); ?></li><!-- End Post-->
            <?php endwhile; endif; wp_reset_query(); ?>
        </ul>
        </div><!--End T Slider-->		
    </div><!-- End right-->
</div><!-- End Container -->
<?php get_footer(); ?>