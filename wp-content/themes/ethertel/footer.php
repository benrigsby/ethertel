<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=container div and all content after
 *
 * @package Toolbox
 * @since Toolbox 0.1
 */
?>

	
<div style="clear:both;"></div>	
	<div id="footer">
    	<div class="container">
			<div style="float:left; margin:0; padding:0;">&copy; <?php echo date('Y'); ?> EtherTel Networks. All rights reserved.</div>
            
            <div style="float:right; margin:-12px 0 0 0; padding:0;">Powered By <a href="http://snapshotinteractive.com" target="_blank"><img src="<?php bloginfo("template_url"); ?>/images/ssi-logo.png" class="ssi"/></a></div>
		</div>
	</div>
<?php wp_footer(); ?>

<script>
			jQuery(document).ready(function($) {
			$('#hslider').bjqs({
				animtype      : 'slide',
				height        : 250,
				width         : 960,
				responsive    : true,
				randomstart   : false,
				animduration  : 450, // how fast the animation are
				animspeed     : 5000, // the delay between each slide
				automatic     : true, // automatic
				showcontrols  : true, // show next and prev controls
				showmarkers   : true, // Show individual slide markers
				nexttext	  : '&#9658;',
				prevtext      : '&#9664;'
			});
		});
		
		jQuery(document).ready(function($) {
			$('#tslider').tst({
				animtype      : 'fade',
				height        : 280,
				width         : 300,
				responsive    : true,
				randomstart   : true,
				animduration  : 450, // how fast the animation are
				animspeed     : 6000, // the delay between each slide
				automatic     : true, // automatic
				showcontrols  : false, // show next and prev controls
				showmarkers   : true, // Show individual slide markers
				nexttext	  : '&#9658;',
				prevtext      : '&#9664;'
			});
		});
		
		
		jQuery(document).ready(function($) {
			$('#quotes').xyz({
				animtype      : 'fade',
				height        : 250,
				width         : 900,
				responsive    : true,
				randomstart   : true,
				animduration  : 450, // how fast the animation are
				animspeed     : 6000, // the delay between each slide
				automatic     : true, // automatic
				showcontrols  : false, // show next and prev controls
				showmarkers   : true, // Show individual slide markers
				nexttext	  : '&#9658;',
				prevtext      : '&#9664;'
			});
		});
		
		
		</script>	

</body>
</html>