<?php
/**
 * Template Name: Full-width, no sidebar
 * Description: A full-width template with no sidebar
 *
 * @package Toolbox
 * @since Toolbox 0.1
 */

get_header(); ?>

		<div class="container">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>
		<?php edit_post_link(); ?>
	<?php endwhile; // end of the loop. ?>

</div>			

<?php get_footer(); ?>