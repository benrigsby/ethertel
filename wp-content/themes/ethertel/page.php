<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Toolbox
 * @since Toolbox 0.1
 */

get_header(); ?>

<div class="container2">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>
		<?php edit_post_link(); ?>
	<?php endwhile; // end of the loop. ?>

</div>			

<?php get_footer(); ?>