<?php
/**
 * Template Name: Testimonials
 *
 */

get_header(); ?>

<div class="container">
	<div class="words">
        <ul>
        <?php query_posts( array( 'cat'=>2, 'showposts' => -1 ) );
            if ( have_posts() ) : while ( have_posts() ) : the_post();
        ?>
            <li><a href="#"><?php the_content(); ?></a></li>
            <?php  endwhile; endif; wp_reset_query(); ?>
        </ul>
    </div>
</div>			

<?php get_footer(); ?>