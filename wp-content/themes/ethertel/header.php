<?php
/**
 * The Header for our theme.
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'toolbox' ), max( $paged, $page ) );
	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<!-- Place all Javascript BELOW WP_HEAD | Jquery is already being loaded please don't load again...-->
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<!-- Place all Javascript references here if needed...-->
<script type="text/javascript" src="<?php bloginfo("template_url"); ?>/js/site.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/bjqs-1.3.js"></script>

<!-- Place Google Analytics Code Here-->
</head>

<body>

	<div id="header">
    	<div class="container">
        <?php if ( is_front_page() or is_page( 93 ) ) :?>
        	<a href="<?php echo home_url(); ?>"><img src="<?php bloginfo("template_url"); ?>/images/logo.png" class="logo"/></a>
            <div style="float:right; width:200px; margin:15px 0 0 0;">
            	<div class="phone">
            		<div>Please Contact Us:</div>
            		<div><a href="#" class="number">615.550.1626</a></div>
                	<div><a href="/get-started/" style="color:#FFF;">info@ethertel.net</a></div>
                </div>
            	<div class="members" style="margin:50px 20px 0 0px;"><a href="/?page_id=93"><img src="<?php bloginfo("template_url"); ?>/images/icba-logo.png"/></a></div>
            </div>
            
         <?php else :?>
         <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo("template_url"); ?>/images/logo.png" class="logo"/></a>
            <div style="float:right; margin:30px 0 0 0;">
            	<div class="phone">
            		<div>Please Contact Us:</div>
            		<div><a href="#" class="number">615.550.1626</a></div>
                	<div><a href="/get-started" style="color:#FFF;">info@ethertel.net</a></div>
                </div>
            </div>
        <?php endif;?>
        </div><!--End Container-->
	</div><!-- End Header-->
    
    
    <div id="nav">
    	<div class="container">
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</div><!--End Container-->
   </div><!-- End Navigation-->
   
   <?php if ( is_front_page() ) :?>
   <div id="featured">
   		<div class="container">
        	<div id="hslider">
        	<ul class="bjqs">
        	<?php query_posts( array( 'cat'=>'4', 'showposts' => 3 ) );
            	if ( have_posts() ) : while ( have_posts() ) : the_post();
            ?>
            	<li>
                	<?php the_post_thumbnail(); ?>
                   
                </li><!-- End Post-->
            	<?php endwhile; endif; wp_reset_query(); ?>
         </ul>
         </div>
        </div><!--End Container-->
   </div><!-- End Featured-->
   <?php else :?>
   <div id="featured2">
   		<div class="container">
        	<?php while ( have_posts() ) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
				<?php endwhile; // end of the loop. ?>
        </div><!--End Container-->
   </div><!-- End Featured-->
   <?php endif;?>